#!/bin/sh
#
# Script to print out the diffs concatenated into one file
#
#       1) starting revision number (int, short number)
#       2) ending revision number (int, short number)
#       3) output file (html)
#

if [ $# -ne 3 ]
then
    echo "Usage: $0 startRevision endRevision outputFileName"
    echo "       Only the integer short revision number"
    exit 1
fi

fileName=$3

# Start Html
echo "<html><head><title>" > $fileName
echo $fileName | sed 's/^.*\///' | sed 's/\..*$/ Code Kata Report/' >> $fileName
echo "</title>" >> $fileName
echo "
<style>
    .addedLine {
        color: #5CFF7C;
    }
    .removedLine {
        color: #F78398;
    }

</style>
" >> $fileName

echo "</head><body style='background-color:black; color:white;'><pre>" >> $fileName;
echo "<h1>Code Kata Report</h1>" >> $fileName;

(( lastDiff=$2 - 1 ))
for (( i=$1; i<$2; i++ )) ; 
do
    (( next=$i + 1))

    #print test diffs first
    hg diff -U5 -r "$i" -r "$next" -I**Test** | recode ASCII..html4 >> $fileName
    echo -e "\n\n" >> $fileName
    hg diff -U100 -r "$i" -r "$next" -X**Test** -X**.iml -X**.sh -X**.html | recode ASCII..html4 >> $fileName


    if [ $i != $lastDiff ]; then
        echo -e "<font color='yellow'>\n\n" >> $fileName
        echo "******************************************" >> $fileName
        echo "******************************************" >> $fileName
        echo "************ Next Diff *******************" >> $fileName
        echo "******************************************" >> $fileName
        echo "******************************************" >> $fileName
        echo -e "\n\n </font>" >> $fileName
    fi
done

echo "</pre></body></html>" >> $fileName
dos2unix $fileName
sed -i '/^[\+|-]/s/$/<\/span>/g' $fileName
sed -i '/^\+/s/^/<span class="addedLine">/g' $fileName
sed -i '/^-/s/^/<span class="removedLine">/g' $fileName
