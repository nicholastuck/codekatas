package com.nicholas.tuck.katas.minesweeper;

import java.util.ArrayList;
import java.util.List;

public class Minesweeper {

    private final char[][] minefield;

    public Minesweeper(char[][] input) {

        minefield = input;
    }

    public char[][] sweep() {
        for (int rowIndex = 0, minefieldLength = minefield.length; rowIndex < minefieldLength; rowIndex++) {
            char[] row = minefield[rowIndex];
            for (int colIndex = 0, rowLength = row.length; colIndex < rowLength; colIndex++) {
                updateCell(rowIndex, colIndex);
            }
        }
        return minefield;
    }

    private void updateCell(int rowIndex, int colIndex) {
        char[] row = minefield[rowIndex];
        char cell = row[colIndex];
        int numMines = 0;
        if (cell != '*') {

            int topRow = (rowIndex > 0) ? rowIndex -1 : 0;
            int bottomRow = (rowIndex + 1 < minefield.length) ? rowIndex + 1 : rowIndex;
            int leftCol = (colIndex - 1 >= 0) ? colIndex -1 : 0;
            int rightCol = (colIndex + 1 < row.length) ? colIndex +1 : colIndex;
            
            for (int neighborRow = topRow; neighborRow <= bottomRow; neighborRow++){
                for (int neighborCol = leftCol; neighborCol <= rightCol; neighborCol++){
                    if (minefield[neighborRow][neighborCol] == '*'){
                        numMines++;
                    }
                }
            }

            minefield[rowIndex][colIndex] = Character.forDigit(numMines, 10);
        }

    }

    public static List<char[][]> sweepFields(List<char[][]> inputFields) {
        List<char[][]> output = new ArrayList<char[][]>();
        
        for (char[][] field : inputFields) {
            Minesweeper minesweeper = new Minesweeper(field);
            output.add(minesweeper.sweep());
        }
        return output;
    }
}
