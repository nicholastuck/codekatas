package com.nicholas.tuck.katas;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.HashMap;
import java.util.Map;

public class SpellOutCheckAmount {
    
    private static Map<Long, String> numericTextValues = buildNumericTextMap();
    private long dollarAmount;
    private short centsAmount;
    
    SpellOutCheckAmount(BigDecimal value) {
        BigDecimal formattedValue = value.setScale(2, BigDecimal.ROUND_HALF_UP);
        
        dollarAmount = formattedValue.longValue();

        centsAmount = formattedValue.subtract(new BigDecimal(dollarAmount)).multiply(new BigDecimal(100)).shortValue();
    }
    
    @Override
    public String toString() {
        String dollarString = getDollarString(dollarAmount);
        if (dollarString == null) {
            long ones = dollarAmount % 10;
            dollarString = getDollarString(dollarAmount - ones) +"-"+ getDollarString(ones);
        }
        
        return String.format(dollarString + " and %02d/100 dollars", centsAmount);
    }

    private String getDollarString(long dollarAmount) {
        return numericTextValues.get(dollarAmount);
    }
    
    private static Map<Long, String> buildNumericTextMap() {
        Map<Long, String> numericTextValueMap = new HashMap<Long, String>();
        numericTextValueMap.put(0l, "Zero");
        numericTextValueMap.put(1l, "One");
        numericTextValueMap.put(2l, "Two");
        numericTextValueMap.put(3l, "Three");
        numericTextValueMap.put(4l, "Four");
        numericTextValueMap.put(5l, "Five");
        numericTextValueMap.put(6l, "Six");
        numericTextValueMap.put(7l, "Seven");
        numericTextValueMap.put(8l, "Eight");
        numericTextValueMap.put(9l, "Nine");
        numericTextValueMap.put(10l, "Ten");
        numericTextValueMap.put(11l, "Eleven");
        numericTextValueMap.put(12l, "Twelve");
        numericTextValueMap.put(13l, "Thirteen");
        numericTextValueMap.put(14l, "Fourteen");
        numericTextValueMap.put(15l, "Fifteen");
        numericTextValueMap.put(16l, "Sixteen");
        numericTextValueMap.put(17l, "Seventeen");
        numericTextValueMap.put(18l, "Eighteen");
        numericTextValueMap.put(19l, "Nineteen");
        numericTextValueMap.put(20l, "Twenty");
        numericTextValueMap.put(30l, "Thirty");
        numericTextValueMap.put(40l, "Forty");

        return numericTextValueMap;
    }



}
