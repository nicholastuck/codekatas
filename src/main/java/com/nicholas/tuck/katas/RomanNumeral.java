package com.nicholas.tuck.katas;

import java.util.HashMap;
import java.util.Map;

public class RomanNumeral {

    public static Map<Character, Integer> romanNumeralMap =
            new HashMap<Character, Integer>();

    static {
        romanNumeralMap.put('i', 1);
        romanNumeralMap.put('v', 5);
        romanNumeralMap.put('x', 10);
        romanNumeralMap.put('l', 50);
        romanNumeralMap.put('c', 100);
        romanNumeralMap.put('d', 500);
        romanNumeralMap.put('m', 1000);
    }

    public static int convert(String romanNumeral) {
        if (romanNumeral.length() == 0) {
            return 0;
        }

        romanNumeral = romanNumeral.toLowerCase();
        if (romanNumeral.length() == 1) {
            return romanNumeralMap.get(romanNumeral.charAt(0));
        }

        int firstValue = romanNumeralMap.get(romanNumeral.charAt(0));
        int secondValue = romanNumeralMap.get(romanNumeral.charAt(1));
        if (firstValue < secondValue) {
            return secondValue - firstValue + convert(romanNumeral.substring(2));
        } else {
            return firstValue + convert(romanNumeral.substring(1));
        }


    }
}
