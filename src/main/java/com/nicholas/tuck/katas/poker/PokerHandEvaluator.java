package com.nicholas.tuck.katas.poker;

import org.apache.commons.lang.ArrayUtils;

import java.util.HashMap;
import java.util.Map;

public class PokerHandEvaluator {
    private static final Map<Character, Integer> cardRanks = new HashMap<Character, Integer>();

    static {
        cardRanks.put('2', 2);
        cardRanks.put('3', 3);
        cardRanks.put('4', 4);
        cardRanks.put('5', 5);
        cardRanks.put('6', 6);
        cardRanks.put('7', 7);
        cardRanks.put('8', 8);
        cardRanks.put('9', 9);
        cardRanks.put('T', 10);
        cardRanks.put('J', 11);
        cardRanks.put('Q', 12);
        cardRanks.put('K', 13);
        cardRanks.put('A', 14);
    }

    public int evaluate(String[] hand1, String[] hand2) {
        int winnerValue = 0;

        winnerValue = evaluateNumberOfPairsWinner(hand1, hand2);
        if (winnerValue == 0) {
            winnerValue = calculatePairWinner(hand1, hand2);
            if (winnerValue == 0) {
                winnerValue = calculateHighCardWinner(hand1, hand2);
            }
        }

        return winnerValue;
    }

    private int evaluateNumberOfPairsWinner(String[] hand1, String[] hand2) {
        int hand1Pairs = evaluateNumberOfPairs(hand1);
        int hand2Pairs = evaluateNumberOfPairs(hand2);
        return Integer.signum(hand1Pairs - hand2Pairs);
    }

    private int calculatePairWinner(String[] hand1, String[] hand2) {
        int highestPairHand1 = calculatePairRank(hand1);
        int highestPairHand2 = calculatePairRank(hand2);

        if (highestPairHand1 == -1 && highestPairHand2 == -1) {
            return 0;
        } else {
            if (highestPairHand1 > highestPairHand2) {
                return 1;
            } else if (highestPairHand1 < highestPairHand2) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    private int evaluateNumberOfPairs(String[] hand) {
        int firstPairRank = calculatePairRank(hand);
        if (firstPairRank != -1) {
            int secondPairRank = calculatePairRank(hand, firstPairRank);
            if (secondPairRank != -1) {
                return 2;
            }
            return 1;
        }
        return 0;
    }
    private int calculatePairRank(String[] hand) {
        return calculatePairRank(hand, 0);
    }
    private int calculatePairRank(String[] hand, int startingRank) {
        for (int i = 0; i < hand.length - 1; i++) {
            int firstCardRank = getCardRank(hand[i]);
            if (firstCardRank > startingRank) {
                for (int j = i + 1; j < hand.length; j++) {
                    int secondCardRank = getCardRank(hand[j]);
                    if (firstCardRank == secondCardRank) {
                        return firstCardRank;
                    }
                }
            }
        }

        return -1;
    }

    private int calculateHighCardWinner(String[] hand1, String[] hand2) {
        while (hand1.length != 0) {
            int hand1HighIndex = highCardIndex(hand1);
            int hand2HighIndex = highCardIndex(hand2);
            int result = Integer.signum(getCardRank(hand1[hand1HighIndex])
                    - getCardRank(hand2[hand2HighIndex]));
            if (result != 0) {
                return result;
            } else {
                hand1 = (String[]) ArrayUtils.removeElement(hand1, hand1[hand1HighIndex]);
                hand2 = (String[]) ArrayUtils.removeElement(hand2, hand2[hand2HighIndex]);
            }
        }
        return 0;
    }


    private int highCardIndex(String[] hand) {
        int highCardIndex = 0;
        for (int index = 0; index < hand.length; index++) {
            if (getCardRank(hand[index]) > getCardRank(hand[highCardIndex])) {
                highCardIndex = index;
            }
        }
        return highCardIndex;
    }

    private int getCardRank(String card) {
        return cardRanks.get(card.charAt(0));
    }
}
