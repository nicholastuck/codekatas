package com.nicholas.tuck.katas.range;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Range {

    private int initial;
    private int end;
    public static final Range NO_INTERSECTION_RANGE = new NO_INTERSECTION();


    public Range(int initial, int end) {
        this.initial = initial;
        this.end = end;
    }

    public boolean contains(int valueToCheck) {
        return valueToCheck >= this.initial && valueToCheck <= end;
    }

    public Range intersection(Range secondRange) {
        int newInitial = Math.max(this.initial, secondRange.getInitial());
        int newEnd = Math.min(this.end, secondRange.getEnd());

        if (newInitial > this.end || newInitial > secondRange.getEnd()
                || newEnd < this.initial || newEnd < secondRange.getInitial()){
            return NO_INTERSECTION_RANGE;
        }

        return new Range(newInitial, newEnd);
    }

    public int getInitial() {
        return initial;
    }

    public int getEnd() {
        return end;
    }

    @Override
    public boolean equals(Object otherRange) {
        if (otherRange == NO_INTERSECTION_RANGE) {
            return false;
        }
        return EqualsBuilder.reflectionEquals(this, otherRange);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    private static class NO_INTERSECTION extends Range {

        public static final String UNSUPPORTED_MESSAGE = "Can't call on NO_INTERSECTION";

        public NO_INTERSECTION(int initial, int end) {
            super(initial, end);
        }

        public NO_INTERSECTION() {
            this(0,0);
        }

        @Override
        public boolean contains(int valueToCheck) {
            throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
        }

        @Override
        public Range intersection(Range secondRange) {
            throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
        }

        @Override
        public int getInitial() {
            throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
        }

        @Override
        public int getEnd() {
            throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
        }

        @Override
        public boolean equals(Object otherRange) {
            return EqualsBuilder.reflectionEquals(this, otherRange);
        }
    }
}
