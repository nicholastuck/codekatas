package com.nicholas.tuck.katas;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class SpellOutCheck {

    private long dollars;
    private long cents;
    private static Map<Long, String> dollarNameMap;

    static {
        dollarNameMap = new HashMap<Long, String>();
        dollarNameMap.put((long) 0, "Zero");
        dollarNameMap.put((long) 1, "One");
        dollarNameMap.put((long) 2, "Two");
        dollarNameMap.put((long) 3, "Three");
        dollarNameMap.put((long) 4, "Four");
        dollarNameMap.put((long) 5, "Five");
        dollarNameMap.put((long) 6, "Six");
        dollarNameMap.put((long) 7, "Seven");
        dollarNameMap.put((long) 8, "Eight");
        dollarNameMap.put((long) 9, "Nine");
        dollarNameMap.put((long) 10, "Ten");
        dollarNameMap.put((long) 11, "Eleven");
        dollarNameMap.put((long) 12, "Twelve");
        dollarNameMap.put((long) 13, "Thirteen");
        dollarNameMap.put((long) 14, "Fourteen");
        dollarNameMap.put((long) 15, "Fifteen");
        dollarNameMap.put((long) 16, "Sixteen");
        dollarNameMap.put((long) 17, "Seventeen");
        dollarNameMap.put((long) 18, "Eighteen");
        dollarNameMap.put((long) 19, "Nineteen");
        dollarNameMap.put((long) 20, "Twenty");
        dollarNameMap.put((long) 30, "Thirty");
        dollarNameMap.put((long) 40, "Forty");
        dollarNameMap.put((long) 50, "Fifty");
        dollarNameMap.put((long) 60, "Sixty");
        dollarNameMap.put((long) 70, "Seventy");
        dollarNameMap.put((long) 80, "Eighty");
        dollarNameMap.put((long) 90, "Ninety");
        dollarNameMap.put((long) 100, "Hundred");

    }

    public SpellOutCheck(BigDecimal value) {
        long totalCents = Math.round(value.floatValue() * 100);
        dollars = totalCents / 100;
        cents = totalCents % 100;
    }
    

    @Override
    public String toString() {
        String dollarString = buildDollarString();
        return String.format("%s and %02d/100 dollars",
                dollarString, cents);
    }

    private String buildDollarString() {
        String dollarString = dollarNameMap.get(dollars);



        if(dollarString == null) {
            long onesDigit = dollars % 10;
            dollarString = dollarNameMap.get(dollars - onesDigit) + "-" + dollarNameMap.get(onesDigit);
        }
        return dollarString;
    }

}
