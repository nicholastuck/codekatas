package com.nicholas.tuck.katas;

import java.util.ArrayList;
import java.util.List;

public class BowlingGame {
    private List<Integer> rolls = new ArrayList<Integer>();
    
    public void roll(int pins) {
        rolls.add(pins);
    }
    
    public int score() {
        int score = 0;
        boolean firstBall = true;
        int currentFrame = 1;

        for (int i=0; i<rolls.size(); i++) {
            score += rolls.get(i);

            if (firstBall) {
                firstBall = false;
                if (shouldCalculateStrike(i, currentFrame)) {
                    score += rolls.get(i + 1);
                    score += rolls.get(i + 2);
                    firstBall = true;
                    currentFrame++;
                }
            } else {
                //Check for spares
                if (rolls.get(i-1) + rolls.get(i) == 10 && currentFrame != 10) {
                    score += rolls.get(i + 1);
                }
                firstBall = true;
                currentFrame++;
            }
        }
        return score;
    }

    private boolean shouldCalculateStrike(int currentRoll, int currentFrame) {
        return rolls.get(currentRoll) == 10 && currentFrame < 10;
    }

}
