package com.nicholas.tuck.katas;

public class TestTemplateClass {
    
    public int addOne (int argument) {
        return ++argument;
    }
}
