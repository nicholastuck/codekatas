package com.nicholas.tuck.katas;

import java.util.HashMap;
import java.util.Map;

public class SpellOutAmount {

    public static Map<String, Integer> numberToDigit =
            new HashMap<String, Integer>();

    static {
        numberToDigit.put("one", 1);
        numberToDigit.put("two", 2);
        numberToDigit.put("three", 3);
        numberToDigit.put("four", 4);
        numberToDigit.put("five", 5);
        numberToDigit.put("six", 6);
        numberToDigit.put("seven", 7);
        numberToDigit.put("eight", 8);
        numberToDigit.put("nine", 9);
        numberToDigit.put("ten", 10);
        numberToDigit.put("eleven", 11);
        numberToDigit.put("twelve", 12);
        numberToDigit.put("thirteen", 13);
        numberToDigit.put("fifteen", 15);
        numberToDigit.put("thousand", 1000);
        numberToDigit.put("hundred", 100);
        numberToDigit.put("twenty", 20);
        numberToDigit.put("thirty", 30);
        numberToDigit.put("forty", 40);
        numberToDigit.put("fifty", 50);
        numberToDigit.put("sixty", 60);
        numberToDigit.put("seventy", 70);
        numberToDigit.put("eighty", 80);
        numberToDigit.put("ninety", 90);
    }

    public static double convert(String stringAmount) {
        stringAmount = stringAmount.toLowerCase();
        String[] amountWords = stringAmount.split(" ", 3);

        if (amountWords.length == 1) {
            return getNumberForWord(stringAmount);
        }

        if (amountWords.length == 2 && !amountWords[1].equals("and")) {
            if (amountWords[1].equals("dollars")) {
                return getNumberForWord(amountWords[0]);
            }
            return getNumberForWord(amountWords[0]) *
                    getNumberForWord(amountWords[1]);
        }

        double firstValue = getNumberForWord(amountWords[0]);
        double secondValue = 1;
        if (!amountWords[1].equals("and")) {
            secondValue = getNumberForWord(amountWords[1]);
        }

        return firstValue * secondValue + convert(amountWords[2]);

    }

    private static double getNumberForWord(String word) {
        if (numberToDigit.get(word) != null) {
            return numberToDigit.get(word);
        }

        if (word.contains("teen")) {
            return 10 + getNumberForWord(word.substring(0,
                    word.indexOf("teen")));
        }

        if (word.contains("-")) {
            int dashIndex = word.indexOf("-");
            return getNumberForWord(word.substring(0, dashIndex)) +
                    getNumberForWord(word.substring(dashIndex + 1));
        }

        if (word.contains("/100")) {
            return getCentsForWord(word);
        }
        return 0;
    }

    private static double getCentsForWord(String word) {
        return Integer.parseInt(word.substring(0, word.indexOf("/100"))) /
                100.0;
    }
}
