package com.nicholas.tuck.katas;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TemplateEngine {


    public static String evaluate(String template, Map<String, String> variableMap) {
        String returnString = template;
        Pattern keyPattern = Pattern.compile("\\{\\$.+?\\}");
        Matcher keyMatcher = keyPattern.matcher(template);
        while(keyMatcher.find()) {
            String keyString = keyMatcher.group(0);
            keyString = keyString.substring(2, keyString.length() - 1);
            String variable = variableMap.get(keyString);
            if (variable == null) {
                throw new IllegalArgumentException("No mapping found for key: " + keyString);
            }
            returnString = returnString.replaceFirst("\\{\\$.+?\\}", variable);
        }
        return returnString;
    }
}
