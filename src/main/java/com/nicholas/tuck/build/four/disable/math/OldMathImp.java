package com.nicholas.tuck.build.four.disable.math;

public class OldMathImp implements MathInterface {

    @Override
    public Integer max(int a, int b) {
        if (a > b) {
            return a;
        }

        if (b > a) {
            return b;
        }

        return null;
    }

    @Override
    public Integer min(int a, int b) {
        if (a < b) {
            return a;
        }

        if (b < a) {
            return b;
        }

        return null;
    }
}
