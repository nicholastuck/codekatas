package com.nicholas.tuck.build.four.disable.math;

public class OldMath {

    public Integer max(int a, int b) {

        if (a > b) {
            return a;
        }

        if (b > a) {
            return b;
        }

        return null;
    }

}
