package com.nicholas.tuck.build.four.disable.math;

public class NewMathDecorator implements MathInterface {

    private OldMathImp oldMath = new OldMathImp();

    @Override
    public Integer max(int a, int b) {
        return Math.max(a, b);
    }

    @Override
    public Integer min(int a, int b) {
        return oldMath.min(a, b);
    }
}
