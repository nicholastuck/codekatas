package com.nicholas.tuck.build.four.disable.math;

public class NewMathIfCheck {

    protected static boolean USE_NEW_MATH = true;

    public Integer max(int a, int b) {

        if (USE_NEW_MATH) {

            return Math.max(a, b);

        } else {

            if (a > b) {
                return a;
            }

            if (b > a) {
                return b;
            }

            return null;

        }

    }

}
