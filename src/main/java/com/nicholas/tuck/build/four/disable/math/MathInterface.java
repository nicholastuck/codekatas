package com.nicholas.tuck.build.four.disable.math;

public interface MathInterface {

    Integer max(int a, int b);

    Integer min(int a, int b);
}
