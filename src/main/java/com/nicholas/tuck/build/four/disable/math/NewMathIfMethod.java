package com.nicholas.tuck.build.four.disable.math;

public class NewMathIfMethod {

    protected static boolean USE_NEW_MATH = true;

    public Integer max(int a, int b) {

        if (USE_NEW_MATH) {
            return maxNewMath(a, b);
        } else {
            return maxOldMath(a, b);
        }

    }

    private Integer maxNewMath(int a, int b) {
        return Math.max(a, b);
    }

    private Integer maxOldMath(int a, int b) {
        if (a > b) {
            return a;
        }

        if (b > a) {
            return b;
        }

        return null;
    }

}
