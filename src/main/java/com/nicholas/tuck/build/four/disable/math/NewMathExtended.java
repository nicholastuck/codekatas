package com.nicholas.tuck.build.four.disable.math;

public class NewMathExtended extends OldMath {

    @Override
    public Integer max(int a, int b) {
        return Math.max(a, b);
    }




    public static void main(String[] args) {
//        OldMath mathLibrary = new OldMath();
        OldMath mathLibrary = new NewMathExtended();

        mathLibrary.max(5, Integer.MIN_VALUE);  // because I can
    }

}
