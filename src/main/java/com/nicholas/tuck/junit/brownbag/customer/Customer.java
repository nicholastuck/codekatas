package com.nicholas.tuck.junit.brownbag.customer;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Customer {

    public static final int EMPTY_CUSTOMER_AGE = -1;

    private int age = EMPTY_CUSTOMER_AGE;
    private String firstName;
    private String lastName;
    private int numberOfItemsPurchased;

    public Customer() { }
    public Customer(int age, String firstName, String lastName, int numberOfItemsPurchased) {
        this.age = age;
        this.firstName = firstName;
        this.lastName = lastName;
        this.numberOfItemsPurchased = numberOfItemsPurchased;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getNumberOfItemsPurchased() {
        return numberOfItemsPurchased;
    }

    public void setNumberOfItemsPurchased(int numberOfItemsPurchased) {
        this.numberOfItemsPurchased = numberOfItemsPurchased;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public boolean equals(Object rightHandSide) {
        return EqualsBuilder.reflectionEquals(this, rightHandSide);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
