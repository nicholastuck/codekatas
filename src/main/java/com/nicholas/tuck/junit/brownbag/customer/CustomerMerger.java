package com.nicholas.tuck.junit.brownbag.customer;

public class CustomerMerger {

    public Customer mergeCustomers(Customer firstCustomer, Customer secondCustomer) {
        Customer mergedCustomer;
        if (mergingAnEmptyCustomer(firstCustomer, secondCustomer)) {
            mergedCustomer = mergeEmptyCustomer(firstCustomer, secondCustomer);
        } else {
            mergedCustomer = mergeTwoCustomers(firstCustomer, secondCustomer);
        }

        return mergedCustomer;
    }

    private Customer mergeTwoCustomers(Customer firstCustomer, Customer secondCustomer) {
        Customer mergedCustomer = new Customer();
        mergedCustomer.setAge(Math.max(firstCustomer.getAge(), secondCustomer.getAge()));
        mergedCustomer.setNumberOfItemsPurchased(firstCustomer.getNumberOfItemsPurchased() + secondCustomer.getNumberOfItemsPurchased());

        mergedCustomer.setFirstName(findLongestFirstName(firstCustomer, secondCustomer));
        mergedCustomer.setLastName(findLongestLastName(firstCustomer, secondCustomer));

        return mergedCustomer;
    }

    private String findLongestFirstName(Customer firstCustomer, Customer secondCustomer) {
        return getLongestString(firstCustomer.getFirstName(), secondCustomer.getFirstName());
    }
    private String findLongestLastName(Customer firstCustomer, Customer secondCustomer) {
        return getLongestString(firstCustomer.getLastName(), secondCustomer.getLastName());
    }
    private static String getLongestString(String string1, String string2) {
        if (string1.length() >= string2.length()) {
            return string1;
        } else {
            return string2;
        }
    }

    private boolean mergingAnEmptyCustomer(Customer firstCustomer, Customer secondCustomer) {
        return firstCustomer.getAge() == Customer.EMPTY_CUSTOMER_AGE || secondCustomer.getAge() == Customer.EMPTY_CUSTOMER_AGE;
    }

    private Customer mergeEmptyCustomer(Customer firstCustomer, Customer secondCustomer) {
       if (firstCustomer.getAge() == Customer.EMPTY_CUSTOMER_AGE) {
            return secondCustomer;
        } else {
            return firstCustomer;
        }
    }

}
