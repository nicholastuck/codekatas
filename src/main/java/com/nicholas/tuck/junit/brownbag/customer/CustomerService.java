package com.nicholas.tuck.junit.brownbag.customer;

public class CustomerService {

    private CustomerMerger customerMerger;

    public CustomerService(CustomerMerger customerMerger) {
        this.customerMerger = customerMerger;
    }

    public Customer mergeCustomers(Customer... customersToMerge) {
        Customer mergedCustomer = customersToMerge[0];
        int numberOfCustomers = customersToMerge.length;
        for (int i = 1; i < numberOfCustomers; i++) {
            mergedCustomer = customerMerger.mergeCustomers(mergedCustomer, customersToMerge[i]);
        }

        return mergedCustomer;
    }
}
