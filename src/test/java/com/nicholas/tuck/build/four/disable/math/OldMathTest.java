package com.nicholas.tuck.build.four.disable.math;

import org.junit.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class OldMathTest {

    private OldMath oldMath = new OldMath();

    @Test
    public void maxShouldReturnAppropriately() throws Exception {
        assertThat(oldMath.max(3, 5), is(5));
        assertThat(oldMath.max(5, 3), is(5));
    }

}
