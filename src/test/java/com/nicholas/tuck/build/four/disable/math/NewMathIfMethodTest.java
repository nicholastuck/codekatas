package com.nicholas.tuck.build.four.disable.math;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

public class NewMathIfMethodTest {

    private NewMathIfMethod newMath = new NewMathIfMethod();

    @Test
    public void maxWithOldMathShouldReturnAppropriately() throws Exception {
        NewMathIfMethod.USE_NEW_MATH = false;

        assertThat(newMath.max(3, 5), is(5));
        assertThat(newMath.max(5, 3), is(5));
        assertThat(newMath.max(5, 5), is(nullValue()));
    }

    @Test
    public void maxWithNewMathShouldReturnAppropriately() throws Exception {
        NewMathIfMethod.USE_NEW_MATH = true;

        assertThat(newMath.max(3, 5), is(5));
        assertThat(newMath.max(5, 3), is(5));
        assertThat(newMath.max(5, 5), is(5));
    }




}
