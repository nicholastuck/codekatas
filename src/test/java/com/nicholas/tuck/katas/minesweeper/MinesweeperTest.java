package com.nicholas.tuck.katas.minesweeper;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class MinesweeperTest {

    public static final char[][] TWO_BY_TWO_ABOVE_LEFT_INPUT = {{'*', '.'},
            {'.', '.'}};
    public static final char[][] TWO_BY_TWO_ABOVE_LEFT_OUTPUT = {{'*', '1'},
            {'1', '1'}};
    public static final char[][] THREE_BY_THREE_SURRENDER_INPUT = {{'*', '*', '*'},
            {'*', '.', '*'},
            {'*', '*', '*'}};
    public static final char[][] THREE_BY_THREE_SURRENDER_OUTPUT = {{'*', '*', '*'},
            {'*', '8', '*'},
            {'*', '*', '*'}};

    @Test
    public void shouldLoadNoMines() {
        char[][] input = new char[2][2];
        input[0] = new char[] {'.', '.'};
        input[1] = new char[] {'.', '.'};

        Minesweeper minesweeper = new Minesweeper(input);
        char[][] output = minesweeper.sweep();
        assertThat(output, notNullValue());
        assertThat(output[0], is(new char[] {'0', '0'}));
        assertThat(output[1], is(new char[] {'0', '0'}));
    }

    @Test
    public void shouldLoadAllMines() {
        char[][] input = new char[2][2];
        input[0] = new char[] {'*', '*'};
        input[1] = new char[] {'*', '*'};

        Minesweeper minesweeper = new Minesweeper(input);
        char[][] output = minesweeper.sweep();
        assertThat(output, notNullValue());
        assertThat(output[0], is(new char[] {'*', '*'}));
        assertThat(output[1], is(new char[] {'*', '*'}));
    }

    @Test
    public void cellNextToSingleMineShouldBeOne() {
        char[][] input = {{'*', '.'}};

        Minesweeper minesweeper = new Minesweeper(input);
        char[][] output = minesweeper.sweep();
        assertThat(output[0], is(new char[] {'*', '1'}));
    }

    @Test
    public void cellWithMinesOnLeftAndRightShouldBeTwo() {
        char[][] input = {{'*', '.', '*'}};

        Minesweeper minesweeper = new Minesweeper(input);
        char[][] output = minesweeper.sweep();
        assertThat(output[0], is(new char[] {'*', '2', '*'}));
    }

    @Test
    public void cellsWithMineAboveShouldBeOne() {
        char[][] input = {{'*'},
                          {'.'}};

        Minesweeper minesweeper = new Minesweeper(input);
        char[][] output = minesweeper.sweep();
        assertThat(output, is(new char[][] {{'*'}, {'1'}}));
    }

    @Test
    public void cellsWithMineBelowShouldBeOne() {
        char[][] input = {{'.'},
                          {'*'}};

        Minesweeper minesweeper = new Minesweeper(input);
        char[][] output = minesweeper.sweep();
        assertThat(output, is(new char[][] {{'1'}, {'*'}}));
    }

    @Test
    public void cellsWithMinesAboveAndBelowShouldBeTwo() {
        char[][] input = {{'*'},
                          {'.'},
                          {'*'}};

        Minesweeper minesweeper = new Minesweeper(input);
        char[][] output = minesweeper.sweep();
        assertThat(output, is(new char[][] {{'*'}, {'2'}, {'*'}}));
    }

    @Test
    public void cellsWithMineAboveAndLeftShouldBeOne() {
        Minesweeper minesweeper = new Minesweeper(TWO_BY_TWO_ABOVE_LEFT_INPUT);
        char[][] output = minesweeper.sweep();
        assertThat(output, is(TWO_BY_TWO_ABOVE_LEFT_OUTPUT));
    }

    @Test
    public void cellSurroundedByMinesShouldBeEight() {
        Minesweeper minesweeper = new Minesweeper(THREE_BY_THREE_SURRENDER_INPUT);
        char[][] output = minesweeper.sweep();
        assertThat(output, is(THREE_BY_THREE_SURRENDER_OUTPUT));
    }

    @Test
    public void complexFieldShouldBeSweptCorrectly() {
        char[][] input ={{'*', '*', '.'},
                         {'.', '.', '*'},
                         {'.', '.', '*'},
                         {'*', '.', '*'}};
        char[][] expectedOutput =   {{'*', '*', '2'},
                                     {'2', '4', '*'},
                                     {'1', '4', '*'},
                                     {'*', '3', '*'}};

        Minesweeper minesweeper = new Minesweeper(input);
        char[][] output = minesweeper.sweep();
        assertThat(output, is(expectedOutput));
    }

    @Test
    public void sweepFieldsShouldSweepMultipleFields() {
        List<char[][]> inputFields = Arrays.asList(TWO_BY_TWO_ABOVE_LEFT_INPUT, THREE_BY_THREE_SURRENDER_INPUT);
        List<char[][]> outputFields = Minesweeper.sweepFields(inputFields);
        
        assertThat(outputFields.size(), is(equalTo(inputFields.size())));
        assertThat(outputFields.get(0), is(TWO_BY_TWO_ABOVE_LEFT_OUTPUT));
        assertThat(outputFields.get(1), is(THREE_BY_THREE_SURRENDER_OUTPUT));
    }

    
}
