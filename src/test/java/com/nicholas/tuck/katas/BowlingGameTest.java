package com.nicholas.tuck.katas;

import com.nicholas.tuck.katas.BowlingGame;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class BowlingGameTest {
    
    private BowlingGame instance = new BowlingGame();

    @Test
    public void shouldCalculateScoreForZeroGame() {
        gutterRest(20);
        assertThat(instance.score(), is(0));
    }
    
    @Test
    public void shouldCalculateScoreOfOne() {
        instance.roll(1);
        gutterRest(19);
        assertThat(instance.score(), is(1));
    }
    
    @Test
    public void shouldCalculateStrike() {
        instance.roll(10);
        instance.roll(1);

        gutterRest(17);

        assertThat(instance.score(), is(12));
    }
    
    @Test
    public void strikeShouldAddNextTwoRolls() {
        instance.roll(10);
        instance.roll(1);
        instance.roll(1);
        gutterRest(16);
        assertThat(instance.score(), is(14));
    }

    @Test
    public void spareShouldTakeOneRoll() {
        instance.roll(1);
        instance.roll(9);
        instance.roll(1);
        gutterRest(17);
        assertThat(instance.score(), is(12));
    }

    @Test
    public void calculateOfSpareStrikeShouldAdd10BonusPins() {
        instance.roll(1);
        instance.roll(9);
        instance.roll(10);
        gutterRest(16);
        assertThat(instance.score(), is(30));
    }

    @Test
    public void tenthFrameStrike() {
        gutterRest(18);
        instance.roll(10);
        instance.roll(1);
        instance.roll(0);

        assertThat(instance.score(), is(11));
    }

    @Test
    public void tenthFrameSpareScoreShouldNotBeTreatedSpecial() {
        gutterRest(18);
        instance.roll(4);
        instance.roll(6);
        instance.roll(1);

        assertThat(instance.score(), is(11));
    }

    @Test
    public void allStrikeGameShouldScore300() {
        for (int i = 0; i < 12; i++){
            instance.roll(10);
        }
        
        assertThat(instance.score(), is(300));
    }

    private void gutterRest(int rolls) {
        for (int i = 0; i < rolls; i++){
            instance.roll(0);
        }
    }
}

