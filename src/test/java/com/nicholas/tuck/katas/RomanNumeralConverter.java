package com.nicholas.tuck.katas;

import java.util.HashMap;
import java.util.Map;

public class RomanNumeralConverter {

    private static final Map<Character, Integer>
            numeralMap =
            new HashMap<Character, Integer>();

    static {
        numeralMap.put('i', 1);
        numeralMap.put('v', 5);
        numeralMap.put('x', 10);
        numeralMap.put('l', 50);
        numeralMap.put('c', 100);
        numeralMap.put('d', 500);
        numeralMap.put('m', 1000);

    }

    public static int convert(String numeral) {
        if(numeral.equals(""))
            throw new IllegalArgumentException();

        numeral = numeral.toLowerCase();


        int result = 0;

        for (int i = 0; i < numeral.length(); ++i) {
            Character currentChar = numeral.charAt(i);
            int currentValue = numeralMap.get(currentChar);

            if (i + 1 < numeral.length()) {
                Character nextChar = numeral.charAt(i + 1);
                int nextValue = numeralMap.get(nextChar);

                if (currentValue < nextValue) {
                    result += nextValue - currentValue;
                    ++i;
                } else {
                    result += currentValue;
                }
            } else {
                result += currentValue;
            }

        }
        return result;
    }
}

