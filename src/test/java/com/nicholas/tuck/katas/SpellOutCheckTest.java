package com.nicholas.tuck.katas;

import org.junit.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.math.BigDecimal;

public class SpellOutCheckTest {

    @Test
    public void decimalValueOfZeroShouldPrintZero() {
        SpellOutCheck checkSpellerOuter = new SpellOutCheck(new BigDecimal(0));
        assertThat(checkSpellerOuter.toString(), is("Zero and 00/100 dollars"));
    }
    
    @Test
    public void zeroDollarsAndSomeCentsShouldPrintCorrectly() {
        SpellOutCheck spellOutAgain = new SpellOutCheck(new BigDecimal("0.13"));
        assertThat(spellOutAgain.toString(), is("Zero and 13/100 dollars"));
    }

    @Test
    public void zeroDollarsAndPartialCentsShouldRoundUp() {
        SpellOutCheck spellOutCheck = new SpellOutCheck(new BigDecimal("0.139"));
        assertThat(spellOutCheck.toString(), is("Zero and 14/100 dollars"));
    }

    @Test
    public void zeroDollarsAndPartialCentsShouldRoundDown() {
        SpellOutCheck spellOutCheck = new SpellOutCheck(new BigDecimal("0.133"));
        assertThat(spellOutCheck.toString(), is("Zero and 13/100 dollars"));
    }

    @Test
    public void zeroDollarsAndPartialCentsShouldRoundHalfUp() {
        SpellOutCheck spellOutCheck = new SpellOutCheck(new BigDecimal("0.135"));
        assertThat(spellOutCheck.toString(), is("Zero and 14/100 dollars"));
    }

    @Test
    public void oneDollarsShouldPrintCorrectly() {
        SpellOutCheck spellOutCheck = new SpellOutCheck(new BigDecimal("1"));
        assertThat(spellOutCheck.toString(), is("One and 00/100 dollars"));
    }

    @Test
    public void oneDollarsAndSomeCentsShouldPrintCorrectly() {
        SpellOutCheck spellOutCheck = new SpellOutCheck(new BigDecimal("1.25"));
        assertThat(spellOutCheck.toString(), is("One and 25/100 dollars"));
    }

    @Test
    public void twoDollarsAndSomeCentsShouldPrintCorrectly() {
        SpellOutCheck spellOutCheck = new SpellOutCheck(new BigDecimal("2.25"));
        assertThat(spellOutCheck.toString(), is("Two and 25/100 dollars"));
    }

    @Test
    public void ninetyNineNineShouldRoundToADollar() {
        SpellOutCheck spellOutCheck = new SpellOutCheck(new BigDecimal("0.999"));
        assertThat(spellOutCheck.toString(), is("One and 00/100 dollars"));
    }

    @Test
    public void threeDollarsShouldPrintCorrectly() {
        SpellOutCheck spellOutCheck = new SpellOutCheck(new BigDecimal("3"));
        assertThat(spellOutCheck.toString(), is("Three and 00/100 dollars"));
    }
    
    @Test
    public void fourDollarsShouldPrintCorrectly() {
        SpellOutCheck spellOutCheck = new SpellOutCheck(new BigDecimal("4"));
        assertThat(spellOutCheck.toString(), is("Four and 00/100 dollars"));
    }

    @Test
    public void shouldHandleFiveThroughTen() {
        assertThat(new SpellOutCheck(new BigDecimal("5")).toString(), is("Five and 00/100 dollars"));
        assertThat(new SpellOutCheck(new BigDecimal("6")).toString(), is("Six and 00/100 dollars"));
        assertThat(new SpellOutCheck(new BigDecimal("7")).toString(), is("Seven and 00/100 dollars"));
        assertThat(new SpellOutCheck(new BigDecimal("8")).toString(), is("Eight and 00/100 dollars"));
        assertThat(new SpellOutCheck(new BigDecimal("9")).toString(), is("Nine and 00/100 dollars"));
        assertThat(new SpellOutCheck(new BigDecimal("10")).toString(), is("Ten and 00/100 dollars"));
    }
    
    @Test
    public void shouldHandleElevenThroughTwenty() {
        assertThat(new SpellOutCheck(new BigDecimal("11")).toString(), is("Eleven and 00/100 dollars"));
        assertThat(new SpellOutCheck(new BigDecimal("12")).toString(), is("Twelve and 00/100 dollars"));
        assertThat(new SpellOutCheck(new BigDecimal("13")).toString(), is("Thirteen and 00/100 dollars"));
        assertThat(new SpellOutCheck(new BigDecimal("14")).toString(), is("Fourteen and 00/100 dollars"));
        assertThat(new SpellOutCheck(new BigDecimal("15")).toString(), is("Fifteen and 00/100 dollars"));
        assertThat(new SpellOutCheck(new BigDecimal("16")).toString(), is("Sixteen and 00/100 dollars"));
        assertThat(new SpellOutCheck(new BigDecimal("17")).toString(), is("Seventeen and 00/100 dollars"));
        assertThat(new SpellOutCheck(new BigDecimal("18")).toString(), is("Eighteen and 00/100 dollars"));
        assertThat(new SpellOutCheck(new BigDecimal("19")).toString(), is("Nineteen and 00/100 dollars"));
        assertThat(new SpellOutCheck(new BigDecimal("20")).toString(), is("Twenty and 00/100 dollars"));
    }

    @Test
    public void shouldHandleTwentyOne() {
        assertThat(new SpellOutCheck(new BigDecimal("21")).toString(), is("Twenty-One and 00/100 dollars"));
    }

    @Test
    public void shouldHandleTwentyTwo() {
        assertThat(new SpellOutCheck(new BigDecimal("22")).toString(), is("Twenty-Two and 00/100 dollars"));
    }

    @Test
    public void shouldHandleThirtyThree() {
        assertThat(new SpellOutCheck(new BigDecimal("33")).toString(), is("Thirty-Three and 00/100 dollars"));
    }

    @Test
    public void shouldHandleFortyThroughNinety() {
        assertThat(new SpellOutCheck(new BigDecimal("41")).toString(), is("Forty-One and 00/100 dollars"));
        assertThat(new SpellOutCheck(new BigDecimal("51")).toString(), is("Fifty-One and 00/100 dollars"));
        assertThat(new SpellOutCheck(new BigDecimal("61")).toString(), is("Sixty-One and 00/100 dollars"));
        assertThat(new SpellOutCheck(new BigDecimal("71")).toString(), is("Seventy-One and 00/100 dollars"));
        assertThat(new SpellOutCheck(new BigDecimal("81")).toString(), is("Eighty-One and 00/100 dollars"));
        assertThat(new SpellOutCheck(new BigDecimal("91")).toString(), is("Ninety-One and 00/100 dollars"));
    }

    //@Test
    public void shouldHandle100() {
        assertThat(new SpellOutCheck(new BigDecimal("100")).toString(), is("One Hundred and 00/100 dollars"));
    }


}
