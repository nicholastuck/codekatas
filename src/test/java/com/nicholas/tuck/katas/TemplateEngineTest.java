package com.nicholas.tuck.katas;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

// Template Engine Code Kata
// https://sites.google.com/site/tddproblems/all-problems-1/Template-engine
public class TemplateEngineTest {

    private Map<String, String> variableMap = new HashMap<String, String>();

    @Test
    public void simpleSingleTemplateShouldEvaluateCorrectly() throws Exception {
        variableMap.put("name", "Nick");
        String returnedString = TemplateEngine.evaluate("Hello {$name}", variableMap);
        assertThat(returnedString, is(equalTo("Hello Nick")));
    }

    @Test
    public void simpleSingleVarWithMultipleTemplateShouldEvaluateCorrectly() throws Exception {
        variableMap.put("name", "Nick");
        String returnedString = TemplateEngine.evaluate("{$name}, Hello {$name}", variableMap);
        assertThat(returnedString, is(equalTo("Nick, Hello Nick")));
    }

    @Test
    public void multipleVarsWithSingleTemplateShouldEvaluateCorrectly() throws Exception {
        variableMap.put("firstName", "Nick");
        variableMap.put("lastName", "Tuck");
        String returnedString = TemplateEngine.evaluate("Howdy Mr. {$lastName}", variableMap);
        assertThat(returnedString, is(equalTo("Howdy Mr. Tuck")));
    }

    @Test
    public void multipleVarsWithMultipleTemplatesShouldEvaluateCorrectly() throws Exception {
        variableMap.put("firstName", "Nick");
        variableMap.put("lastName", "Tuck");
        String returnedString = TemplateEngine.evaluate("Howdy {$firstName} {$lastName}", variableMap);
        assertThat(returnedString, is(equalTo("Howdy Nick Tuck")));
    }

    @Test
    public void noVariablesAndNoTemplateShouldReturnString() throws Exception {
        String returnedString = TemplateEngine.evaluate("simple test", variableMap);
        assertThat(returnedString, is(equalTo("simple test")));
    }

    @Test(expected = IllegalArgumentException.class)
    public void templateWithNoCorrespondingVarShouldThrowIllegalArgumentException() throws Exception {
        TemplateEngine.evaluate("Missing {$variable} exception", variableMap);
    }

    @Test
    public void complexTemplateShouldMapInnerTemplateOnly() throws Exception {
        variableMap.put("name", "Nick");
        String returnedString = TemplateEngine.evaluate("Hello ${$name}}", variableMap);
        assertThat(returnedString, is(equalTo("Hello $Nick}")));
    }
}
