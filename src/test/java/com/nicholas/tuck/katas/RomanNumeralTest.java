package com.nicholas.tuck.katas;

import com.nicholas.tuck.katas.RomanNumeral;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class RomanNumeralTest {

    @Test
    public void shouldConvertIto1() {
        assertThat(RomanNumeral.convert("i"), is(equalTo(1)));
    }

    @Test
    public void shouldConvertUppercaseIto1() {
        assertThat(RomanNumeral.convert("I"), is(equalTo(1)));
    }

    @Test
    public void shouldConvertVto5() {
        assertThat(RomanNumeral.convert("v"), is(equalTo(5)));
    }

    @Test
    public void shouldConvertXto10() {
        assertThat(RomanNumeral.convert("x"), is(equalTo(10)));
    }

    @Test
    public void shouldConvertLto50() {
        assertThat(RomanNumeral.convert("l"), is(equalTo(50)));
    }

    @Test
    public void shouldConvertCto100() {
        assertThat(RomanNumeral.convert("C"), is(equalTo(100)));
    }

    @Test
    public void shouldConvertDto500() {
        assertThat(RomanNumeral.convert("D"), is(equalTo(500)));
    }

    @Test
    public void shouldConvertMto1000() {
        assertThat(RomanNumeral.convert("m"), is(equalTo(1000)));
    }

    @Test
    public void shouldConvertIIto2() {
        assertThat(RomanNumeral.convert("ii"), is(equalTo(2)));
    }

    @Test
    public void shouldConvertVIto6() {
        assertThat(RomanNumeral.convert("vi"), is(equalTo(6)));
    }

    @Test
    public void shouldConvertIVto4() {
        assertThat(RomanNumeral.convert("iv"), is(equalTo(4)));
    }

    @Test
    public void shouldConvertIXto9() {
        assertThat(RomanNumeral.convert("ix"), is(equalTo(9)));
    }

    @Test
    public void shouldConvertIIIto3() {
        assertThat(RomanNumeral.convert("iii"), is(equalTo(3)));
    }

    @Test
    public void shouldConvertXIVto14() {
        assertThat(RomanNumeral.convert("xiV"), is(equalTo(14)));
    }

    @Test
    public void shouldConvertXVIIIto18() {
        assertThat(RomanNumeral.convert("xViii"), is(equalTo(18)));
    }

    @Test
    public void shouldConvertMCMIIIto1903() {
        assertThat(RomanNumeral.convert("MCMIII"), is(equalTo(1903)));
    }


}
