package com.nicholas.tuck.katas;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class RomanNumeralConverterTest {
    
    @Test
    public void shouldConvertIto1() {
        assertThat(RomanNumeralConverter.convert("i"),
                is(equalTo(1)));
    }

    @Test
    public void shouldConvertVto5() {
        assertThat(RomanNumeralConverter.convert("v"),
                is(equalTo(5)));
    }
    
    @Test
    public void shouldConvertXto10() {
        assertThat(RomanNumeralConverter.convert("x"), 
                is(equalTo(10)));
    }

    @Test
    public void shouldConvertLto50() {
        assertThat(RomanNumeralConverter.convert("l"),
                is(equalTo(50)));
    }

    @Test
    public void shouldConvertCto100() {
        assertThat(RomanNumeralConverter.convert("c"),
                is(equalTo(100)));
    }

    @Test
    public void shouldConvertDto500() {
        assertThat(RomanNumeralConverter.convert("d"),
                is(equalTo(500)));
    }

    @Test
    public void shouldConvertMto1000() {
        assertThat(RomanNumeralConverter.convert("m"),
                is(equalTo(1000)));
    }
    
    @Test
    public void shouldBeCaseInsensitive() {
        assertThat(RomanNumeralConverter.convert("X"),
                is(equalTo(10)));
    }
    
    @Test
    public void shouldConvertIIto2() {
        assertThat(RomanNumeralConverter.convert("ii"),
                is(equalTo(2)));
    }

    @Test
    public void shouldConvertIIIto3() {
        assertThat(RomanNumeralConverter.convert("III"),
                is(equalTo(3)));
    }
    
    @Test
    public void shouldConvertIVto4() {
        assertThat(RomanNumeralConverter.convert("IV"),
                is(equalTo(4)));
    }

    @Test
    public void shouldConvertXLto40() {
        assertThat(RomanNumeralConverter.convert("XL"),
                is(equalTo(40)));
    }
    
    @Test
    public void shouldConvertXIXto19() {
        assertThat(RomanNumeralConverter.convert("XIX"),
                is(equalTo(19)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldHandleEmptyNumeral() {
        RomanNumeralConverter.convert("");
    }

    @Test
    public void shouldConvertMCMIIIto1903() {
        assertThat(RomanNumeralConverter.convert("MCMIII"),
            is(equalTo(1903)));
    }

}
