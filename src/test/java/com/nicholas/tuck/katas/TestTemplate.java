package com.nicholas.tuck.katas;

import com.nicholas.tuck.katas.TestTemplateClass;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class TestTemplate {

    private TestTemplateClass testTemplate;

    @Before
    public void setUp() throws Exception {
        testTemplate = new TestTemplateClass();
    }

    @Test
    public void shouldAddOneToZero() {
        int responseInt = testTemplate.addOne(0);
        assertThat(responseInt, is(equalTo(1)));
    }
}
