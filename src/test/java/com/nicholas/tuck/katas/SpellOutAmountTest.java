package com.nicholas.tuck.katas;

import com.nicholas.tuck.katas.SpellOutAmount;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class SpellOutAmountTest {

    @Test
    public void shouldConvertOneTo1(){
        assertThat(SpellOutAmount.convert("one"), equalTo(1.0));
    }

    @Test
    public void shouldConvertTwoTo2(){
        assertThat(SpellOutAmount.convert("two"), equalTo(2.0));
    }
    @Test
    public void shouldConvertThreeTo3(){
        assertThat(SpellOutAmount.convert("three"), equalTo(3.0));
    }
    @Test
    public void shouldConvertFourTo4(){
        assertThat(SpellOutAmount.convert("four"), equalTo(4.0));
    }
    @Test
    public void shouldConvertFiveTo5(){
        assertThat(SpellOutAmount.convert("five"), equalTo(5.0));
    }
    @Test
    public void shouldConvertSixTo6(){
        assertThat(SpellOutAmount.convert("six"), equalTo(6.0));
    }
    @Test
    public void shouldConvertSevenTo7(){
        assertThat(SpellOutAmount.convert("seven"), equalTo(7.0));
    }
    @Test
    public void shouldConvertEightTo8(){
        assertThat(SpellOutAmount.convert("eight"), equalTo(8.0));
    }
    @Test
    public void shouldConvertNineTo9(){
        assertThat(SpellOutAmount.convert("nine"), equalTo(9.0));
    }
    @Test
    public void shouldConvertTenTo10(){
        assertThat(SpellOutAmount.convert("ten"), equalTo(10.0));
    }
    @Test
    public void shouldConvertRandomCaseSevenTo7(){
        assertThat(SpellOutAmount.convert("SeVEn"), equalTo(7.0));
    }
    @Test
    public void shouldConvert7Thousand(){
        assertThat(SpellOutAmount.convert("SeVEn thousand"), equalTo(7000.0));
    }
    @Test
    public void shouldConvert5Thousand(){
        assertThat(SpellOutAmount.convert("five thousand"), equalTo(5000.0));
    }

    @Test
    public void shouldConvert12(){
        assertThat(SpellOutAmount.convert("twelve"), equalTo(12.0));
    }
    @Test
    public void shouldConvert17(){
        assertThat(SpellOutAmount.convert("seventeen"), equalTo(17.0));
    }
    @Test
    public void shouldConvert1700(){
        assertThat(SpellOutAmount.convert("seventeen hundred"), equalTo(1700.0));
    }
    @Test
    public void shouldConvertThirteen(){
        assertThat(SpellOutAmount.convert("thirteen"), equalTo(13.0));
    }
    @Test
    public void shouldConvertTwentyThree(){
        assertThat(SpellOutAmount.convert("twenty-three"), equalTo(23.0));
    }
    @Test
    public void shouldConvertThirtyFour(){
        assertThat(SpellOutAmount.convert("thirty-four"), equalTo(34.0));
    }
    @Test
    public void shouldConvertFortyFive(){
        assertThat(SpellOutAmount.convert("forty-five"), equalTo(45.0));
    }
    @Test
    public void shouldConvertFiftySix(){
        assertThat(SpellOutAmount.convert("fifty-six"), equalTo(56.0));
    }
    @Test
    public void shouldConvertSixtySeven(){
        assertThat(SpellOutAmount.convert("sixty-seven"), equalTo(67.0));
    }
    @Test
    public void shouldConvertSeventyEight(){
        assertThat(SpellOutAmount.convert("seventy-eight"), equalTo(78.0));
    }
    @Test
    public void shouldConvertEightyNine(){
        assertThat(SpellOutAmount.convert("eighty-nine"), equalTo(89.0));
    }
    @Test
    public void shouldConvertNinetyOne(){
        assertThat(SpellOutAmount.convert("ninety-one"), equalTo(91.0));
    }

    @Test
    public void shouldConvertFiftyThreeHundred(){
        assertThat(SpellOutAmount.convert("fifty-three hundred"), equalTo(5300.0));
    }

    @Test
    public void shouldConvertTwoThousandFiveHundred(){
        assertThat(SpellOutAmount.convert("two thousand five hundred"),
                equalTo(2500.0));
    }

    @Test
    public void shouldConvertTwoThousandFiveHundredTwentyThree(){
        assertThat(SpellOutAmount.convert("two thousand five hundred twenty-three"),
                equalTo(2523.0));
    }

    @Test
    public void shouldConvert1And2Cents(){
        assertThat(SpellOutAmount.convert("one and 02/100 dollars"),
                equalTo(1.02));
    }

    @Test
    public void shouldConvert3And6Cents(){
        assertThat(SpellOutAmount.convert("three and 06/100 dollars"),
                equalTo(3.06));
    }

    @Test
    public void shouldConvert2523And4Cents(){
        assertThat(SpellOutAmount.convert("Two thousand five hundred twenty-three "
                + "and 04/100 dollars"), equalTo(2523.04));
    }


}
