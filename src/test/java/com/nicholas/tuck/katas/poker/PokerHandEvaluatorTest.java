package com.nicholas.tuck.katas.poker;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class PokerHandEvaluatorTest {
    public static final int HAND2 = -1;
    public static final int HAND1 = 1;
    public static final int TIE = 0;
    private PokerHandEvaluator evaluator = new PokerHandEvaluator();


    
    @Test
    public void testThatHighestCardWins() throws Exception {
        String[] hand1 = {"2H", "3D", "5S", "9C", "KD"};
        String[] hand2 = {"2C", "3H", "4S", "8C", "AH"};
        assertThat(evaluator.evaluate(hand1, hand2), equalTo(HAND2));
    }
    
    @Test
    public void testThatHighestCardWinsNoMatterTheOrder(){
        String[] hand1 = {"2H", "3D", "5S", "9C", "KD"};
        String[] hand2 = {"2C", "3H", "4S", "8C", "AH"};
        assertThat(evaluator.evaluate(hand2,hand1), equalTo(HAND1));
    }

    @Test
    public void testTies() throws Exception {
        String[] hand1 = {"2H", "3D", "5S", "9C", "KD"};
        String[] hand2 = {"2D", "3H", "5C", "9D", "KH"};
        assertThat(evaluator.evaluate(hand1,hand2), equalTo(TIE));
    }

    @Test
    public void testSecondHighCard() throws Exception {
        String[] hand1 = {"2H", "3D", "5S", "9C", "AD"};
        String[] hand2 = {"2C", "3H", "4S", "8C", "AH"};
        assertThat(evaluator.evaluate(hand1,hand2), equalTo(HAND1) );
    }

    @Test
    public void pairShouldWinOverHighCard() throws Exception {
        String[] hand1 = {"2H", "3D", "5S", "9C", "AD"};
        String[] hand2 = {"2C", "3H", "4S", "JC", "JH"};
        assertThat(evaluator.evaluate(hand1,hand2), equalTo(HAND2) );
    }

    @Test
    public void pairTiesShouldEvaluateHighCard() throws Exception {
        String[] hand1 = {"2H", "2D", "5S", "QC", "AD"};
        String[] hand2 = {"2C", "2H", "4S", "JC", "QH"};
        assertThat(evaluator.evaluate(hand1,hand2), equalTo(HAND1) );
    }

    @Test
    public void highestPairShouldWin() throws Exception {
        String[] hand1 = {"2H", "2D", "5S", "QC", "AD"};
        String[] hand2 = {"3C", "3H", "4S", "JC", "QH"};
        assertThat(evaluator.evaluate(hand1,hand2), equalTo(HAND2) );
    }

    @Test
    public void twoPairShouldBeatPair() throws Exception {
        String[] hand1 = {"2H", "2D", "5S", "5C", "AD"};
        String[] hand2 = {"AC", "AH", "4S", "JC", "QH"};
        assertThat(evaluator.evaluate(hand1,hand2), equalTo(HAND1) );
    }





}
