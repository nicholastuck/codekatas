package com.nicholas.tuck.katas;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class SpellOutCheckAmountTest {

    //assertEquals("Two thousand five hundred twenty-three and 04/100 dollars",
    // new CheckAmount(new BigDecimal("2523.04")).toString());

    @Test
    public void checkAmount0ShouldPrintZero() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(0)).toString(), is(equalTo("Zero and 00/100 dollars")));
    }
    
    @Test
    public void checkAmount1centShouldPrintZeroAndOneCent() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(0.01)).toString(), is(equalTo("Zero and 01/100 dollars")));
    }
    
    @Test
    public void checkAmount20centsShouldPrintZeroAnd20Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(0.20)).toString(), is(equalTo("Zero and 20/100 dollars")));
    }

    @Test
    public void checkAmount20Point7CentsShouldPrintZeroAnd20Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(0.207)).toString(), is(equalTo("Zero and 21/100 dollars")));
    }

    @Test
    public void checkAmount1DollarShouldPrintOneAnd00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(1.00)).toString(), is(equalTo("One and 00/100 dollars")));
    }

    @Test
    public void checkAmount1DollarShouldPrintOneAnd01Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(1.01)).toString(), is(equalTo("One and 01/100 dollars")));
    }

    @Test
    public void checkAmount2DollarShouldPrintOneAnd01Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(2.01)).toString(), is(equalTo("Two and 01/100 dollars")));
    }
    
    @Test
    public void checkAmount10DollarShowPrintTenAnd00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(10.00)).toString(), is(equalTo("Ten and 00/100 dollars")));
    }
    
    @Test
    public void returnThreeDollarsWith99Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(3.9939999)).toString(), is(equalTo("Three and 99/100 dollars")));
    }

    @Test
    public void returnFourDollarsWith00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(4.00)).toString(), is(equalTo("Four and 00/100 dollars")));
    }

    @Test
    public void returnFiveDollarsWith00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(5.00)).toString(), is(equalTo("Five and 00/100 dollars")));
    }

    @Test
    public void returnSixDollarsWith00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(6.00)).toString(), is(equalTo("Six and 00/100 dollars")));
    }

    @Test
    public void returnSevenDollarsWith00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(7.00)).toString(), is(equalTo("Seven and 00/100 dollars")));
    }

    @Test
    public void returnEightDollarsWith00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(8.00)).toString(), is(equalTo("Eight and 00/100 dollars")));
    }

    @Test
    public void returnNineDollarsWith00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(9.00)).toString(), is(equalTo("Nine and 00/100 dollars")));
    }

    @Test
    public void returnElevenDollarsWith00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(11.00)).toString(), is(equalTo("Eleven and 00/100 dollars")));
    }

    @Test
    public void returnTwelveDollarsWith00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(12.00)).toString(), is(equalTo("Twelve and 00/100 dollars")));
    }

    @Test
    public void returnThirteenDollarsWith00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(13.00)).toString(), is(equalTo("Thirteen and 00/100 dollars")));
    }

    @Test
    public void returnFourteenDollarsWith00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(14.00)).toString(), is(equalTo("Fourteen and 00/100 dollars")));
    }
    @Test
    public void returnFifteenDollarsWith00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(15.00)).toString(), is(equalTo("Fifteen and 00/100 dollars")));
    }
    @Test
    public void returnSixteenDollarsWith00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(16.00)).toString(), is(equalTo("Sixteen and 00/100 dollars")));
    }
    @Test
    public void returnSeventeenDollarsWith00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(17.00)).toString(), is(equalTo("Seventeen and 00/100 dollars")));
    }
    @Test
    public void returnEighteenDollarsWith00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(18.00)).toString(), is(equalTo("Eighteen and 00/100 dollars")));
    }
    @Test
    public void returnNineteenDollarsWith00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(19.00)).toString(), is(equalTo("Nineteen and 00/100 dollars")));
    }
    @Test
      public void returnTwentyDollarsWith04Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(20.04)).toString(), is(equalTo("Twenty and 04/100 dollars")));
    }
    @Test
    public void returnTwentyOneDollarsWith00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(21.00)).toString(), is(equalTo("Twenty-One and 00/100 dollars")));
    }

    @Test
    public void returnThirtyDollarsWith00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(30.00)).toString(), is(equalTo("Thirty and 00/100 dollars")));
    }

    @Test
    public void returnThirtyTwoDollarsWith00Cents() throws Exception {
        assertThat(new SpellOutCheckAmount(new BigDecimal(32.00)).toString(), is(equalTo("Thirty-Two and 00/100 dollars")));
    }
}
