package com.nicholas.tuck.katas.range;

import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class RangeTest {

    @Test
    public void singleValueRangeShouldContainSameInteger() {
        Range range = new Range(3,3);
        assertThat(range.contains(3), is(true));
    }

    @Test
    public void singleValueRangeShouldNotContainDifferentInt() {
        Range range = new Range(3,3);
        assertThat(range.contains(4), is(false));
    }

    @Test
    public void twoValueRangeShouldContainSecondInt() {
        Range range = new Range(3,4);
        assertThat(range.contains(4), is(true));
    }

    @Test
    public void twoValueRangeShouldNotContainValueOutsideOfRange() {
        Range range = new Range(3,4);
        assertThat(range.contains(1), is(false));
    }

    @Test
    public void rangeShouldContainValueWithinIt() {
        Range range = new Range(-3,7);
        assertThat(range.contains(4), is(true));
    }
    
    @Test
    public void singleRangeShouldIntersectWithSelfExactly() {
        Range initialRange = new Range(5,5);
        Range secondRange = initialRange;
        Range expectedIntersection = new Range(5,5);
        assertThat(initialRange.intersection(secondRange), is(equalTo(expectedIntersection)));
    } 
    
    @Test
    public void twoValueRangeShouldIntersectWithSingleValueRangeCorrectly() {
        Range initialRange = new Range(4,5);
        Range secondRange = new Range(5,5);
        Range expectedIntersection = secondRange;
        assertThat(initialRange.intersection(secondRange), is(equalTo(expectedIntersection)));
    }
    
    @Test
    public void twoValueRangeShouldIntersectWith1ValueOverlapTwoValueRangeCorrectly() {
        Range initialRange = new Range(4,5);
        Range secondRange = new Range(5,6);
        Range expectedIntersection = new Range(5,5);
        assertThat(initialRange.intersection(secondRange), is(equalTo(expectedIntersection)));
    }

    @Test
    public void twoValueRangeShouldIntersectWith2ValueOverlap3ValueRangeCorrectly() {
        Range initialRange = new Range(4,6);
        Range secondRange = new Range(4,5);
        Range expectedIntersection = new Range(4,5);
        assertThat(initialRange.intersection(secondRange), is(equalTo(expectedIntersection)));
    }

    @Test
    public void oneRangeEncompassingAnotherShouldIntersectTheSmaller() {
        Range initialRange = new Range(4,12);
        Range secondRange = new Range(7,9);
        Range expectedIntersection = new Range(7,9);
        assertThat(initialRange.intersection(secondRange), is(equalTo(expectedIntersection)));
    }

    @Test
    public void bigRangeOverlappingAnotherBigRangeShouldIntersectCorrectly() {
        Range initialRange = new Range(-50, 73);
        Range secondRange = new Range(27, 1040);
        Range expectedIntersection = new Range(27,73);
        assertThat(initialRange.intersection(secondRange), is(equalTo(expectedIntersection)));
    }
    
    @Test
    public void rangesThatDontIntersectShouldReturnNoIntersectRange() {
        Range initialRange = new Range(7, 9);
        Range secondRange = new Range(15, 20);
        Range expectedIntersection = Range.NO_INTERSECTION_RANGE;
        assertThat(initialRange.intersection(secondRange), is(equalTo(expectedIntersection)));
    }
    
    @Test
    public void rangesThatDontIntersectShouldNotEqual00Range() {
        Range initialRange = new Range(7, 9);
        Range secondRange = new Range(15, 20);
        Range unexpectedIntersection = new Range(0,0);
        assertThat(initialRange.intersection(secondRange), is(not(equalTo(unexpectedIntersection))));
    }

    @Test
    public void noIntersectRangeShouldThrowExceptionOnMethodCalls() {
        boolean hasThrown = false;
        try { Range.NO_INTERSECTION_RANGE.getEnd(); } catch (Exception e){
            hasThrown = true;
        }
        if (!hasThrown) Assert.fail("Didn't throw on getEnd");

        hasThrown = false;
        try { Range.NO_INTERSECTION_RANGE.getInitial(); } catch (Exception e){
            hasThrown = true;
        }
        if (!hasThrown) Assert.fail("Didn't throw on getInitial");

        hasThrown = false;
        try { Range.NO_INTERSECTION_RANGE.contains(-1); } catch (Exception e){
            hasThrown = true;
        }
        if (!hasThrown) Assert.fail("Didn't throw on contains");

        hasThrown = false;
        try { Range.NO_INTERSECTION_RANGE.intersection(Range.NO_INTERSECTION_RANGE); } catch (Exception e){
            hasThrown = true;
        }
        if (!hasThrown) Assert.fail("Didn't throw on intersection");
    }
}
