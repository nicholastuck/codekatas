package com.nicholas.tuck.junit.brownbag.customer;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.Mockito.*;

import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {

    @Mock private CustomerMerger customerMerger;
    /*@InjectMocks*/ private CustomerService customerService; // show @InjectMocks later

    @Captor private ArgumentCaptor<Customer> customerCaptor;

    // Supporting constants -- these are common and okay.  Don't clutter the tests with exact details, make them read to the point
    public static final Customer CUSTOMER_A = new Customer(20, "Nick", "Tuck", 5);
    public static final Customer CUSTOMER_B = new Customer(20, "Nicholas", "Tuck", 0);
    public static final Customer CUSTOMERS_AB_MERGED = new Customer(20, "Nicholas", "Tuck", 5);
    public static final Customer CUSTOMER_C = new Customer(25, "Nick", "Tuck", 15);
    public static final Customer CUSTOMERS_ABC_MERGED = new Customer(25, "Nicholas", "Tuck", 20);

    @Before
     public void setUp() throws Exception {
        customerService = new CustomerService(customerMerger);
    }

    @Test
//    @Ignore
    public void mergingListOfCustomersWithCustomerMergerShouldMergeTwice() {
        Customer customerA = new Customer(20, "Nick", "Tuck", 5);
        Customer customerB = new Customer(20, "Nicholas", "Tuck", 0);
        Customer customerC = new Customer(25, "Nick", "Tuck", 15);
        Customer expectedCustomer = new Customer(25, "Nicholas", "Tuck", 20);

        // These lines are also now testing CustomerMerger... We don't want this.  We only want to test CustomerService.
        CustomerService newCustomerService = new CustomerService(new CustomerMerger());
        Customer mergedCustomer = newCustomerService.mergeCustomers(customerA, customerB, customerC);

        assertThat(mergedCustomer, is(equalTo(expectedCustomer)));  // break something in CustomerMerger and show this test fails
    }

    @Test
    public void merging3CustomersShouldCallCustomerMergerCorrectly() {
        Customer customerA = new Customer(20, "Nick", "Tuck", 5);
        Customer customerB = new Customer(20, "Nicholas", "Tuck", 0);
        Customer firstMergeReturn = new Customer(20, "Nicholas", "Tuck", 5);

        Customer customerC = new Customer(25, "Nick", "Tuck", 15);
        Customer secondMergeReturn = new Customer(25, "Nicholas", "Tuck", 20);

        when(customerMerger.mergeCustomers(customerA, customerB)).thenReturn(firstMergeReturn);
        when(customerMerger.mergeCustomers(firstMergeReturn, customerC)).thenReturn(secondMergeReturn);
        
        Customer finalMergedCustomer = customerService.mergeCustomers(customerA, customerB, customerC);

        assertThat(finalMergedCustomer, is(secondMergeReturn));
    }

    @Test
    public void merging3CustomersCleanerTest() {
        when(customerMerger.mergeCustomers(CUSTOMER_A, CUSTOMER_B)).thenReturn(CUSTOMERS_AB_MERGED);
        when(customerMerger.mergeCustomers(CUSTOMERS_AB_MERGED, CUSTOMER_C)).thenReturn(CUSTOMERS_ABC_MERGED);

        Customer finalMergedCustomer = customerService.mergeCustomers(CUSTOMER_A, CUSTOMER_B, CUSTOMER_C);

        assertThat(finalMergedCustomer, is(CUSTOMERS_ABC_MERGED));
    }

    @Test
    public void merging4CustomersShouldCallCustomerMerge3Times() {
        Customer customer = new Customer(47, "Indiana", "Jones", 2);

        customerService.mergeCustomers(customer, customer, customer, customer);

        verify(customerMerger, times(3)).mergeCustomers(Mockito.any(Customer.class), Mockito.any(Customer.class));
    }

    @Test
    public void merging4CustomersCleaner() {
        customerService.mergeCustomers(CUSTOMER_A, CUSTOMER_B, CUSTOMER_C, CUSTOMER_A);
        verify(customerMerger, times(3)).mergeCustomers(Mockito.any(Customer.class), Mockito.any(Customer.class));
    }

    // Show @InjectMocks

    @Test
    public void mergingThreeCustomersShouldCallMergerTwiceWithExpectedArguments() {
        when(customerMerger.mergeCustomers(CUSTOMER_A, CUSTOMER_B)).thenReturn(CUSTOMERS_AB_MERGED);

        customerService.mergeCustomers(CUSTOMER_A, CUSTOMER_B, CUSTOMER_C);

        verify(customerMerger).mergeCustomers(customerCaptor.capture(), eq(CUSTOMER_C));    // comment on the eq()
        assertThat(customerCaptor.getValue(), is(CUSTOMERS_AB_MERGED));
    }
}
