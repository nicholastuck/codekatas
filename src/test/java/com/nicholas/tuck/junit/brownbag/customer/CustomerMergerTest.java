package com.nicholas.tuck.junit.brownbag.customer;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.internal.runners.JUnit38ClassRunner;
import org.junit.rules.ExpectedException;
import org.junit.runners.JUnit4;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class CustomerMergerTest {

    private CustomerMerger customerMerger;      // instance (common name but not really an encouraged practice)

    public static final Customer EMPTY_CUSTOMER = new Customer();

//    @BeforeClass
    @Before     // every test
    public void setUp() throws Exception {
        customerMerger = new CustomerMerger();
    }
//    @After  // every test
//    @AfterClass

    @Test
    public void mergingCustomerWithEmptyCustomerShouldEqualTheOnlyCustomer() {
        Customer realCustomer = new Customer(99, "George", "Washington", 0);

        Customer mergedCustomer = customerMerger.mergeCustomers(realCustomer, EMPTY_CUSTOMER);
        assertThat(mergedCustomer, is(equalTo(realCustomer)));      // show how helpful overwriting toString is
        Assert.assertEquals(realCustomer, mergedCustomer);          // compared junit assert (expected is backwards, not easy to read naturally, limited etc)
    }

    @Test
    public void mergingCustomersOrderShouldNotMatter() {
        Customer realCustomer = new Customer(99, "George", "Washington", 0);

        Customer mergedCustomer1 = customerMerger.mergeCustomers(realCustomer, EMPTY_CUSTOMER);
        Customer mergedCustomer2 = customerMerger.mergeCustomers(EMPTY_CUSTOMER, realCustomer);

        assertThat(mergedCustomer1, is(equalTo(mergedCustomer2)));
    }

    @Test
    public void mergingTwoCustomersShouldMergeNamesByLongestNames() {
        Customer anthonyS = new Customer(23, "Anthony", "S", 0);
        Customer tonyStark = new Customer(23, "Tony", "Stark", 0);
        Customer expectedMergedCustomer = new Customer(23, "Anthony", "Stark", 0);

        Customer mergedCustomer = customerMerger.mergeCustomers(anthonyS, tonyStark);

        assertThat(mergedCustomer, is(equalTo(expectedMergedCustomer)));    // show how fails without equals override
    }

    @Test
    public void mergingTwoCustomersShouldUseOldestAge() {
        Customer youngJackNicholson = new Customer(21, "Jack", "Nicholson", 0);
        Customer oldJackNicholson = new Customer(75, "Jack", "Nicholson", 0);
//        Customer expectedMergedCustomer = new Customer(75, "Jack", "Nicholson", 0);

        Customer mergedCustomer = customerMerger.mergeCustomers(youngJackNicholson, oldJackNicholson);

//        assertThat(mergedCustomer, is(equalTo(expectedMergedCustomer)));

        // sometimes just keep things simple
        assertThat(mergedCustomer.getAge(), is(equalTo(oldJackNicholson.getAge())));
    }

    @Test
    public void mergingTwoCustomersShouldAddNumberOfProductsPurchased() {
        Customer ritchieRichVisaCard = new Customer(13, "Ritchie", "Rich", 425);
        Customer ritchieRichMasterCard = new Customer(13, "Ritchie", "Rich", 225);
        Customer expectedMergedCustomer = new Customer(13, "Ritchie", "Rich", 650);

        Customer mergedCustomer = customerMerger.mergeCustomers(ritchieRichVisaCard, ritchieRichMasterCard);

        assertThat(mergedCustomer, is(equalTo(expectedMergedCustomer)));

//        assertThat(mergedCustomer.getNumberOfItemsPurchased(), is(greaterThan(ritchieRichVisaCard.getNumberOfItemsPurchased())));
//        assertThat(mergedCustomer.getNumberOfItemsPurchased(), is(greaterThan(ritchieRichMasterCard.getNumberOfItemsPurchased())));

//        assertThat(mergedCustomer.getNumberOfItemsPurchased(), is(allOf(greaterThan(ritchieRichVisaCard.getNumberOfItemsPurchased()),
//                                                                        greaterThan(ritchieRichMasterCard.getNumberOfItemsPurchased()))));

                                                                // show other matchers - double close to, hasItem,
    }

    @Test
    public void shouldMergeTwoComplexCustomersAppropriately() {
        Customer bruceWayne = new Customer(34, "Bruce", "Wayne", 150);
        Customer batman = new Customer(190, "Batman", "The", 800);
        Customer expectedMergedCustomer = new Customer(190, "Batman", "Wayne", 950);

        Customer mergedCustomer = customerMerger.mergeCustomers(bruceWayne, batman);

        assertThat(mergedCustomer, is(equalTo(expectedMergedCustomer)));
    }

    @Test(expected = NullPointerException.class)
    public void mergingNullCustomerShouldThrowNullPointerException() {
        customerMerger.mergeCustomers(new Customer(0,"First", "Last", 0), null);
    }

    @Test(expected = NullPointerException.class)
    public void mergingNullCustomerShouldThrowNullPointerExceptionWithAStackTrace() {
        try {
            customerMerger.mergeCustomers(new Customer(0,"First", "Last", 0), null);
        } catch (NullPointerException nullPointerException) {
            assertThat(nullPointerException.getStackTrace(), is(not(nullValue())));        // not can be used on all matchers
            throw nullPointerException;     // throw it to let junit handle it, and the test be clearer an exception is what is being tested here
        }
    }

    // show removing @Before
}
