//package com.nicholas.tuck.katas
//
//import spock.lang.Specification
//
//class BowlingGameSpec extends Specification {
//    def "testing various scores"() {
//        expect:
//        game.score() == score
//
//        where:
//        game | score
//        bowlGame([]) | 0
//        bowlGame([[1]]) | 1
//        bowlGame([[10], [1]]) | 12
//        bowlGame([[10], [1, 1]]) | 14
//        bowlGame([[1, 9], [1, 0]]) | 12
//        bowlGame([[1, 9], [10]]) | 30
//        bowlGame([[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [10, 1, 0]]) | 11
//        bowlGame([[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [6, 4, 1]]) | 11
//        bowlGame([[10], [10], [10], [10], [10], [10], [10], [10], [10], [10, 10, 10]]) | 300
//    }
//
//    def "bowlGame"(frames) {
//        BowlingGame game = new BowlingGame()
//        def numFramesBowled = 0
//
//        for (frame in frames) {
//            for (roll in frame) {
//                game.roll(roll)
//            }
//            ++numFramesBowled
//        }
//
//        for (i in 1..(10 - numFramesBowled)) {
//            game.roll(0)
//            game.roll(0)
//        }
//
//        return game
//    }
//}
